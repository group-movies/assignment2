# Assignment 2 - Agile Software Practice.

Name: Evan Sullivan

## API endpoints.



+ GET api/movies/:id - get the details of a specific movie.
+ POST api/movies/:id/reviews (Auth) - Add a review for a movie.
+ GET api/movies - Get m list of movies from TMDB's Discover endpoint, starting at page n and limit the list size to m.  
+ POST api/:userName/favourites - add a movie to the named user's favourites list.
+ POST /api/users?action=action - Register or authenticate a user - set action to register or login. 
+ GET api/genres - Get a list of movie genres.
+ GET api/movies/tmdb/upcoming (Auth) - Get a list of upcoming movies.
+ GET api/movies/tmdb/trending (Auth) - Get a list of trending movies for the current week.
+ GET api/movies/tmdb/nowPLaying (Auth) - Get a list of movies that are currenlty in cinemas.
+ GET api/movies/:id/recommendations (Auth) - Get a list of movie recommendations based on selected movie
+ PUT /api/users/:id - Update user.

## Automated Testing.

~~~
    √ should return the 2 users and a status 200 (38ms)
    POST /api/users 
      For a register action
        when the payload is correct
          √ should return a 201 status and the confirmation message (109ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (118ms)
      incorrect login detected
        √ user doesn't exist (52ms)
        √ no username
        √ password is wrong (97ms)
        √ no password
      change user dwtails
        √ update the user by its ID
      failed registration
        √ detects missing passsword
        √ detects missing username

         Movies endpoint
    GET /api/movies
      √ should return 10 movies and a status 200 (73ms)
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie
      when the id is invalid
        √ should return the NOT found message
      all endpoints TMDB
        web page endpoints
          √ GET /api/movies/:id 
          √ GET /api/movies/:id  (39ms)
            16 passing (6s)

--------------------------------|---------|----------|---------|---------|------------------------------------
File                            | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
--------------------------------|---------|----------|---------|---------|------------------------------------
All files                       |   65.96 |    41.33 |   63.96 |   74.15 | 
 movies-api-cicd                |     100 |      100 |     100 |     100 | 
  index.js                      |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api            |   23.28 |    13.33 |   27.27 |      30 | 
  tmdb-api.js                   |   23.28 |    13.33 |   27.27 |      30 | 5-13,23-29,35-43,51-60,66-75,80-90
 movies-api-cicd/api/movies     |    65.9 |    37.93 |   53.12 |   79.59 | 
  index.js                      |   62.96 |    35.71 |      50 |   76.74 | 45-46,50-51,55-56,60-61,65-66     
  movieModel.js                 |     100 |      100 |     100 |     100 | 
 movies-api-cicd/api/users      |   88.78 |    63.63 |   91.42 |   90.76 | 
  index.js                      |   86.11 |    69.35 |   86.36 |      85 | 36-44
  userModel.js                  |   94.28 |    54.05 |     100 |     100 | 2-22
 movies-api-cicd/authenticate   |   21.87 |    21.62 |      50 |      25 | 
  index.js                      |   21.87 |    21.62 |      50 |      25 | 6-23
 movies-api-cicd/db             |   83.33 |      100 |      50 |   81.81 | 
  index.js                      |   83.33 |      100 |      50 |   81.81 | 11,14
 movies-api-cicd/errHandler     |      40 |        0 |       0 |      40 | 
  index.js                      |      40 |        0 |       0 |      40 | 4-7
 movies-api-cicd/initialise-dev |     100 |      100 |     100 |     100 | 
  movies.js                     |     100 |      100 |     100 |     100 | 
  users.js                      |     100 |      100 |     100 |     100 | 
 movies-api-cicd/seedData       |      95 |     47.5 |     100 |     100 | 
  index.js                      |   94.73 |     47.5 |     100 |     100 | 2-36
  movies.js                     |     100 |      100 |     100 |     100 | 
--------------------------------|---------|----------|---------|---------|------------------------------------
~~~
  

## Deployments.

https://assignment2-staging-es-ed9d30125e5f.herokuapp.com/api/movies

## Independent Learning 

I couldnt upload to coveralls as there was a token blocking the upload. But i did manage to get the instanbul working to give a report of the coverage.


## Problems
Due to the web app assignment 2 being coded in such a way that the some of the UI was needed, i couldnt test the new endpoints. i could do many tests for usrs and also the movies endpoint but not the Upcoming, trending, recommendations, nowPlaying, Favourites