import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import User from "../../../../api/users/userModel";
import api from "../../../../index";

const expect = chai.expect;
let db;
let user1token;
let user1id;

describe("Users endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  beforeEach(async () => {
    try {
      await User.deleteMany();
      // Register two users
      await request(api).post("/api/users?action=register").send({
        username: "user1",
        password: "test123@",
      });
      await request(api).post("/api/users?action=register").send({
        username: "user2",
        password: "test123@",
      });
    } catch (err) {
      console.error(`failed to Load user test Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close();
  });
  describe("GET /api/users ", () => {
    it("should return the 2 users and a status 200", (done) => {
      request(api)
        .get("/api/users")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(2);
          let result = res.body.map((user) => user.username);
          expect(result).to.have.members(["user1", "user2"]);
          done();
        });
    });
  });

  describe("POST /api/users ", () => {
    describe("For a register action", () => {
      describe("when the payload is correct", () => {
        it("should return a 201 status and the confirmation message", () => {
          return request(api)
            .post("/api/users?action=register")
            .send({
              username: "user3",
              password: "test123@",
            })
            .expect(201)
            .expect({ success: true, msg: 'User successfully created.' });
        });
        after(() => {
          return request(api)
            .get("/api/users")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body.length).to.equal(3);
              const result = res.body.map((user) => user.username);
              expect(result).to.have.members(["user1", "user2", "user3"]);
              user1id = res.body[0]._id
             
            });
        });
      });
    });
    describe("For an authenticate action", () => {
      describe("when the payload is correct", () => {
        it("should return a 200 status and a generated token", () => {
          return request(api)
            .post("/api/users?action=authenticate")
            .send({
              username: "user1",
              password: "test123@",
            })
            .expect(200)
            .then((res) => {
              expect(res.body.success).to.be.true;
              expect(res.body.token).to.not.be.undefined;
              user1token = res.body.token.substring(7);
            });
        });
      });
    });
    describe("incorrect login detected", () => {
     
      it("user doesn't exist", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "paddy6",
          password: "test123@"
        })
        .expect(401)
        .expect({ success: false, msg: 'Authentication failed. User not found.' });
      })
      
      it("no username", () => {
        return request(api)
        .post("/api/users")
        .send({
          password: "test123@"
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
   
     
    
     
     
      it("password is wrong", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "user1",
          password: "ytpeufhg748!"
        })
        .expect(401)
        .expect({ success: false, msg: 'Wrong password.' });
      })
      it("no password", () => {
        return request(api)
        .post("/api/users")
        .send({
          username: "user1"
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
    })
    describe("change user dwtails", () => {
      it("update the user by its ID", () => {
        request(api)
          .put(`/api/users/${user1id}`)
          .send({
            "username": "user189",
            "password": "poiuytrfg3!"
          })
          .expect(200)
          .expect({ msg: 'User Updated Successfully' })
         
      }) 
    })
    describe("failed registration", () => {
     
      it("detects missing passsword", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user1076",
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      it("detects missing username", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          password: "nneiwhfuiewhf!2",
        })
        .expect(400)
        .expect({ success: false, msg: 'Username and password are required.' });
      })
      
      it("the password is too short", async () => {
        try {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user13",
          password: "s"
        })
        .expect(500)
        .expect({ success: false, msg: 'Internal server error.' });
      } catch (error) {
        expect(error.code).to.equal(11000)
        expect(error.name).to.equal('MongoServerError');
      }

     
      it("detects if the username already exists", () => {
        return request(api)
        .post("/api/users?action=register")
        .send({
          username: "user1",
          password: "test456@"
        })
        .expect(500)
        .expect({ success: false, msg: 'Internal server error.' });
      })
    })

    
  
   
    })
  });
});
