import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";

const expect = chai.expect;
let db;

let movieid = 671583
let nonmovieID = 12345678;
var token = "";

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  beforeEach(async () => {
    try {
      await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
    return request(api)
    .post("/api/users?action=authenticate")
    .send({
      username:"user1",
      password:"test123@"
    })
    .expect(200)
    .then((res) => {
      expect(res.body.success).to.be.true
      expect(res.body.token).to.not.be.undefined
      token = res.body.token.substring(7)
     
    })
  })

  afterEach(() => {
    api.close(); // Release PORT 8080
  });

  describe("GET /api/movies ", () => {
    it("should return 10 movies and a status 200", (done) => {
      request(api)
        .get("/api/movies")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body.results).to.be.a("array");
          expect(res.body.results.length).to.equal(10);
          done();
        });
    });
  });

  

   describe("GET /api/movies/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}`)
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("title", movies[0].title);
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999")
          .set("Accept", "application/json")
          .set("Authorization", token)
          .expect("Content-Type", /json/)
          .expect(404)
          .expect({
            status_code: 404,
            message: "The movie you requested could not be found.",
          });
      });
    });
    describe("all endpoints TMDB", () => {
      describe("web page endpoints", () => {
        it("GET /api/movies/:id ", () => {//for a movie in database
          return request(api)
            .get(`/api/movies/${movieid}`)
            .set("Accept", "application/json")
            .set("Authorization", token)
            .expect("Content-Type", /json/)
            .expect(200)
            .then((res) => {
              expect(res.body).to.have.property("title", "Upside-Down Magic");
            });
        })

        it("GET /api/movies/:id ", () => { // for movies not in database
          return request(api)
            .get(`/api/movies/${nonmovieID}`)
            .set("Accept", "application/json")
            .set("Authorization", token)
            .expect(404) 
            .then((res) => {
              expect(res.text).to.contain("The movie you requested could not be found.")
            })
        })

        // it("GET /api/genres", () => {
        //   return request(api)
        //     .get("/api/movies/genres")
        //     .set("Accept", "application/json")
        //     .set("Authorization", token)
        //     .expect("Content-Type", "text/html; charset=utf-8")
        //     .expect(200)
        //     .then((res) => {
        //       expect(res.body.genres).to.be.a("array");
        //     });
        // })

        // it("GET /tmdb/upcoming/", () => {
        //   return request(api) 
        //   .get(`/api/movies/tmdb/upcoming`)
        //     .set("Accept", "application/json")
        //     .set("Authorization", token)
        //     .expect("Content-Type", "text/html; charset=utf-8")
        //     .expect(200)
        // })

        
      })
    })
  });
});
